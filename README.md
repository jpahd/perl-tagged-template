# Perl Tagged Template Extension

This extension provides syntax highlighting for Perl code inside tagged template literals in JavaScript files in Visual Studio Code.

## Features

- Syntax highlighting for Perl code inside tagged template literals in JavaScript files.
- Supports many common Perl syntax elements, including comments, keywords, variables, flow control statements, operators, regular expressions, and module-related keywords.
- Customizable syntax highlighting rules using the TextMate grammar file included in the extension.
- Easy installation and setup using the Visual Studio Code Extension Marketplace.

## Installation

You can install this extension from the Visual Studio Code Extension Marketplace by searching for "Perl Tagged Template Extension" or by following this link: [https://marketplace.visualstudio.com/items?itemName=lybrari.perl-tagged-template](https://marketplace.visualstudio.com/items?itemName=lybrari.perl-tagged-template)

## Usage

Once the extension is installed, it will automatically highlight Perl code inside tagged template literals in JavaScript files.

To customize the syntax highlighting rules, you can modify the TextMate grammar file included in the extension. You can access this file by opening the extension folder in Visual Studio Code and navigating to the `syntaxes` folder.

## Contributing

If you find a bug or have a feature request, please submit an issue on the GitHub repository: [https://gitlab.com/jpahd/perl-tagged-templates](https://github.com/jpahd/perl-tagged-templates)

If you would like to contribute to the development of this extension, please fork the GitHub repository, make your changes, and submit a pull request.

## License

This extension is released under the [MIT License](https://opensource.org/licenses/MIT).
