const test = [parent.addEventListener(() => {
    let a = 1;
    for (let i = 0; i < 10; i++) {
        a += i;
    }

    return a;
})]

const shouldSucceed = perl`
    # This is some Perl code that uses a lot of keywords
    # and should be highlighted correctly
    my $foo = 1;
    my $bar = 2;
    my $baz = 3;

    my $qux = $foo + $bar + $baz;

    print $qux;

    my $quux = "Hello, world!";

    print $quux;

    my $corge = "Hello, world!";

    print $corge;

    my $grault = "Hello, world!";

    print $grault;

    my $garply = "Hello, world!";

    my @arr = (1, 2, 3);

    for my $i (@arr) {
        print $i;
    }

    my $waldo = "Hello, world!";
    
`;