# Change Log

All notable changes to the "perl-template-literals" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [0.0.1]

- Initial release

## [0.0.2]

- Changed repository URL
- Donwgraded the vscode version requirement to 1.76.2

## [0.1.0]

- Got a prototype working
- Main issue at the moment is that Symbols that occur in both JavaScript and Perl are not highlighted in surrounding JavaScript/TypeScript code