export default function perl(strings, ...values) {
  let result = "";
  strings.forEach((str, i) => {
    result += str + (values[i] !== undefined ? values[i] : "");
  });
  return result;
}
